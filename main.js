const { app, BrowserWindow, ipcMain, protocol } = require('electron')
const isDev = require('electron-is-dev')
const path = require('path')
const prepareRenderer = require('electron-next')

async function createWindow () {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1370,
    height: 787,
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false
    }
  })

  // and load the index.html of the app.
  // Open the DevTools.
  // prepareRenderer('')
  // win.webContents.openDevTools()
  // win.loadFile('./out/index.html')
  const startURL = isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, './build/index.html')}`;
  if (isDev){
    win.loadURL(startURL)
    // await prepareRenderer('./', 3000)
    // win.loadFile('./public/index.html')
    win.webContents.openDevTools()
  }else {
    win.webContents.openDevTools()
    win.loadFile('./build/index.html')
    // win.loadFile(startURL)
  }

  //drag and drop funcionalidad para las imagenes
  ipcMain.on('ondragstart', (event, filePath) => {
    console.log('image_path in main js', filePath)
    event.sender.startDrag({
      file: filePath,
      // icon: '/path/to/icon.png'
    })
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
