//lib from https://www.npmjs.com/package/convert-pdf-png
// and modified to work with dpfjs-dist version 2.4.456
import React from 'react'
import PDFJS from 'pdfjs-dist';
import pdfjsWorker from 'pdfjs-dist/build/pdf.worker.entry'
//
PDFJS.GlobalWorkerOptions.workerSrc = pdfjsWorker;

const convertPdfToPng = (
  file,
  config = {
    outputType: 'callback',
    callback: () => {}
  }
) => {
  let fileReader = new FileReader();
  let images = [];
  let currentPage = 1;

  fileReader.onload = async e => {
    const uint8array = new Uint8Array(e.target.result);
    const pdf = await PDFJS.getDocument({ data: uint8array }).promise;

    const getPage = async () => {
      const page = await pdf.getPage(currentPage); // modified this line
      const viewport = page.getViewport({scale: 1.5}); // modified this line
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      const renderContext = { canvasContext: ctx, viewport };
      canvas.height = viewport.height;
      canvas.width = viewport.width;
      await page.render(renderContext).promise;

      canvas.toBlob(async blob => {
        const image = new File([blob], `page-${currentPage}.png`, {
          lastModified: new Date(),
          type: 'image/png'
        });

        images.push(image);
        if (currentPage < pdf.numPages) {
          currentPage++;
          await getPage();
        } else {
          switch (config.outputType) {
            case 'callback':
            default:
              config.callback(images);
              break;
          }
        }
      });
    };
    await getPage(); // modified this line, this one was at line 24

  };

  fileReader.readAsArrayBuffer(file);
};
export default convertPdfToPng
