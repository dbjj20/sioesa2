import React from 'react'
import Main from './components/main/index'

function App() {
  return (
    <div>
      <header className="header is-primary">
          <h1 className="title is-size-3 has-text-grey">
              <span><i className="fas fa-archway" aria-hidden="true"></i> Sioesa</span>
          </h1>
      </header>
      <Main/>
    </div>
  );
}

export default App;
