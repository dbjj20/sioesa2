import React from 'react'
import Head from 'next/head'
import setup from './../lib/db'
import { getStore, addToStore } from './../lib/reduxito'
import "react-datepicker/dist/react-datepicker.css"
import 'tui-image-editor/dist/tui-image-editor.css'
// import ImageEditor from '@toast-ui/react-image-editor'

(async ()=>{
  await setup()
  await getStore(async (s) => {
    let {a, db} = await s.init()
    await addToStore({ dbs: a,  db: db })
  })
})()
export default class MyApp extends React.Component{
  constructor(props){
    super(props)
    this.state = {
    }
  }

  async componentDidMount(){
  }

  render(){
    return(
      <div>
          <Head>
              <meta charSet="UTF-8"/>
              <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
              <link rel="stylesheet" href="css/main.css"/>
              <title>Sioesa</title>
              <link src="fonts/fontawesome-5.13.1/css/all.css" rel="stylesheet"></link>
              <script defer src="fonts/fontawesome-5.13.1/js/all.min.js"></script>
          </Head>
          <header className="header is-primary">
              <h1 className="title is-size-3 has-text-grey">
                  <span><i className="fas fa-archway" aria-hidden="true"></i> Sioesa</span>
              </h1>
          </header>
          <this.props.Component
            {...this.props.pageProps}
          />
      </div>
    )
  }
}
// createDb={(name) => this.createDb(name)}
// addtodbfriends={(obj) => this.addtodbfriends(obj)}
// addtodbLocations={(obj) => this.addtodbLocations(obj)}
