import React from 'react'
import { getPreparedProperties, Base } from '../../lib/utils'
import { getStore } from '../../lib/reduxito'

export default class BackUpView extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      backup_path: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.save = this.save.bind(this)
  }

  async save(){
    await getStore((store) => {
      let initial = new Base(store.db.init)
       initial.update(this.state, store.db, {id: 1})
    })
  }

  async handleChange(e){
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({[name]: target.files[0].path})
    // console.log(target.files[0].path)
  }

  async componentDidMount(){
    await getStore(async (store) => {
      let r = await store.db.init.where("id").equals(1).toArray()
      await this.setState(r[0])
    })
  }

  render(){
    return(
      <div className="container">
        <div className="field is-horizontal">
          <div className="field-label is-small">
            <label className="label">Ruta de Backup</label>
          </div>
          <div className="field-body">
            <div className="field has-addons">
              <div className="file is-medium is-boxed is-centered">
                <input className="button" id="drag" name="backup_path" type="file" webkitdirectory={true} directory={true} multiple={false} onChange={this.handleChange} placeholder=""/>
              </div>
              <button className="button" onClick={this.save}>guardar</button>
            </div>
          </div>
        </div>
        <div className="field is-horizontal">
          <div className="field-label is-small">
            <label className="label">Ruta: </label>
          </div>
          <div className="field-body">
            <div className="field has-addons">
              <div className="file is-medium is-boxed is-centered">
                {this.state.backup_path}
              </div>
            </div>
          </div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
      </div>
    )
  }
}
