import React from 'react'

export default class ConfigView extends React.Component{
  constructor(props){
    super(props)

  }
  render(){
    return(
      <div className="container">
        <div className="field is-horizontal">
          <div className="field-label is-small">
            <label className="label">Ruta de Backup</label>
          </div>
          <div className="field-body">
            <div className="field has-addons">
              <div className="control">
                <input className="input is-small" type="text" placeholder=""/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
