import React from 'react'

export default class Preview extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div id="preview_print">
        <div className="container">
          <h2 className="title is-4">
            Resumen
          </h2>
          <div className="divider"></div>
          <div className="result">
            {/*<!-- page 1 -->*/}

            <hr/>
            <h4 className="number_in_circle">1</h4>
            <hr/>

            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Descripción del inmueble</h2>
              </div>
              <div className="column is-full">
                <p className="has-text-centered is-size-7 has-text-weight-bold">Tasación para la determinación del <span>valor de mercado</span> </p>
              </div>
              <div className="column is-full">
                <p className="has-text-centered is-size-7 has-text-weight-bold">Este informe <span></span> es válido para ser presentado como <span>garantia hipotecaria</span></p>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Fecha informe</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Martes 23 de Junio</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Fecha inspección</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Miercoles 17 de Junio</p>
                  </div>
                </div>
                <div className="columns is-multiline">
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Solicitado por</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Nombre</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Alejandro Jacobo Albojer Molina</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Cédula/RNC</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">402-2056066-4</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Teléfono</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">809-123-3123</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Email</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">micorreopersonal23@hotmail.com</p>
                  </div>
                </div>

                <div className="columns is-multiline">
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Propietario</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Nombre</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Alejandro Jacobo Albojer Molina</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Cédula/RNC</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">402-2056066-4</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Teléfono</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">809-123-3123</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Email</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">micorreopersonal23@hotmail.com</p>
                  </div>
                </div>
                <div className="columns is-multiline">
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Descripción legal</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Tipo de inmueble</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Un solar y un edif. de cuatro niveles de uso comercial</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Libro / Folio</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">23423234/2342332, 23232253,</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Constancia de título</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">0200122807 y 0200051718</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Designación catastral</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">P. No. 312511058866 y S. No. 2, M. No. 185, DC 01.</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Provincia</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">18 - Puerto Plata</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Municipio</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">04 - Licey</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Área total titulada</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">634.5 m²</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Dirección del inmueble</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Calle del Sol, 90, Tienda la Bomba, Puerto Plata Rep. Dom.</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full">
                    <figure className="image is-4by3">
                      <img src="https://www.thehousedesigners.com/house-plans/images/AdvSearch2-7263.jpg"/>
                    </figure>
                  </div>
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Colindancias generales del lote</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Norte</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">P. No. 872 (RESTO)</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Este</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">P. No. 872 (RESTO)</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Sur</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">CALLE PUENTE DE LA 20</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Oeste</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">P. No. 872 (RESTO) y CALLE</p>
                  </div>
                </div>
                <div className="columns is-multiline">
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Coordenadas geográficas del inmueble</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Latitud norte</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7"> 19.450784°</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Longitud Oste</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">-70.704304°</p>
                  </div>
                </div>
                <div className="columns is-multiline">
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Requerido por</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Institución</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Banco Dominicano Popular</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">RNC</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">7683492934</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Sucursal</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Central</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Gerente</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Juan Carlos Alboran Cordero</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Teléfono</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">809-555-4545</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Email</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">jcalboran@popularenlinea.com</p>
                  </div>
                </div>
              </div>
            </div>

            {/*<!-- page 2 -->*/}
            <hr/>
            <h4 className="number_in_circle">2</h4>
            <hr/>

            <div className="divider"></div>

            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Vecindad y/o sector del inmueble</h2>
                <p className="has-text-centered is-size-7 subtitle">(Factores predominantes)</p>
              </div>
              <div className="column is-half">
                <div className="columns is-multiline">
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Locación</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Urbano</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Uso de suelo</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Agricola</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Tipo de edificaciones</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Neoclásica</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Clase social</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Baja</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">% de solares edificados</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">80%</p>
                  </div>
                </div>
              </div>
              <div className="column is-half">
                <div className="columns is-multiline">
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Crecimiento</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Nulo</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Conservación</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Excelente, A</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Cambio de uso</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Poco probable</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Edad aparente</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">15 Años</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Demanda / Oferta</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Baja</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Deseabilidad</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">Alta</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Factores desfavorables</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">LA FALTA DE PARQUEOS PUBLICOS</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Factores favorables</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">AL SECTOR LLEGAN TODAS LAS RUTAS DEL TRANSPORTE PUBLICO URBANO E INTERUBANO</p>
                  </div>
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Otros</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">INMUEBLE UBICADO EN  EL CENTRO HISTORICO DE LA CIUDAD DE SANTIAGO. </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Servicios disponible en el sector y/o zona</h2>
                <p className="has-text-centered is-size-7 subtitle">{` Lejano > 2 km &nbsp; | &nbsp; Cercano < 2 km y > 1 km  &nbsp;|&nbsp;  Proximo < 1 km`}</p>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Vías y áreas públicas</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Asfaltada</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Disponible</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Aceras</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Disponible</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Contenes</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">No</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Área verde</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Si</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Área Deportivas</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Cercano</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Distancia a la vía principal</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Próximo</p>
                  </div>

                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Servicios básicos</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Drenaje sanitario</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Si</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Drenaje pluvial</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Si</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Agua potable</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Si</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Alumbrado público</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Si</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Linea de telefono y cable</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Si</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Est. de alta tensión y/o Bco. transformadores</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Lejano</p>
                  </div>

                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full">
                    <p className="has-text-centered is-size-6">Otros servicios</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Transporte público</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Próximo</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Centros de salud</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Próximo</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Centros educativos</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Próximo</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Oficinas Públicas</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Próximo</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Comercios menores</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Próximo</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Est. de gasolina</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Próximo</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Est. de GLP</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Próximo</p>
                  </div>
                </div>
              </div>
              <div className="column is-full">
                <p className="has-text-centered is-size-6">Vías de acceso</p>
              </div>
              <div className="column is-two-fifths">
                <div className="columns is-multiline">
                  <div className="column is-one-quarter is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Principales</p>
                  </div>
                  <div className="column is-three-quarters is-field">
                    <p className="is-size-7">CALLE DEL SOL</p>
                  </div>
                </div>
              </div>
              <div className="column is-three-fifths">
                <div className="columns is-multiline">
                  <div className="column is-one-quarter is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Secundarias</p>
                  </div>
                  <div className="column is-three-quarters is-field">
                    <p className="is-size-7">CALLE 30 DE MARZO Y CALLE SAN LUIS</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-one-third is-label">
                  <p className="has-text-right is-size-7 has-text-weight-bold">Lugares, sectores y comunidades cercanas y/o colindantes</p>
                </div>
                <div className="column is-two-thirds is-field">
                  <p className="is-size-7">LA HOYA, LOS PEPINES, EL CONGO, BARACOA</p>
                </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Caracteristicas del emplazamiento o solar</h2>
                <p className="has-text-centered is-size-7 subtitle">Longitudes aproximadas levantadas con cinta y/o laser métrico de mano</p>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Área const. 1er nivel</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">507.00 m²</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Long. frente</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">22.64 ml</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Long. fondo prom.</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">33.44 ml</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">(AOCP) = Área ocupada </p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">293.28</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">% Const. / solar</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">79%</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Factor frente / fondo </p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">0.68</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Geometría</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Regular</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Topografía</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Bajo el nivel de la calle</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Ubicación manzana</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Medial</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Uso actual</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Residencial</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Mejor y más alto uso</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Residencial</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Deseabilidad</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">Alta</p>
                  </div>
                </div>
              </div>
              <div className="column is-12">
                <div className="columns is-multiline">
                  <div className="column is-one-third is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Notas extras</p>
                  </div>
                  <div className="column is-two-thirds is-field">
                    <p className="is-size-7">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolores esse illum natus ut iure nihil porro. Libero distinctio beatae impedit.</p>
                  </div>
                </div>
              </div>
            </div>

            {/*<!-- page 3 -->*/}
            <hr/>
            <h4 className="number_in_circle">3</h4>
            <hr/>

            <div className="divider"></div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Análisis del valor comparativa</h2>
              </div>
              <div className="column is-full">
                <p className="has-text-centered is-size-7">Letreros expuestos en la via pública, ventas no realizadas, que no establecen un precio en la zona pero, si nos dan un estimado de los valores máximos ofertados por los dueños de las propiedades, que manejados con criterio profesional podemos determinar el valor máximo promedio del inmueble de interes</p>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">1</p>
                  </div>
                  <div className="column is-11 is-field">
                    <p className="is-size-7">2018, ESPAÑA, As=187.92 M2., Ac=638.93 M2. por RD $15,000,000.00 (ANGEL ROSARIO AL 809-805-3218)</p>
                  </div>
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">2</p>
                  </div>
                  <div className="column is-11 is-field">
                    <p className="is-size-7">2018,  CALLE SALVADOR CUCURULLO, As=218.00 M2., @ RD $29,725.00 C/M2., (F. ABREU AL 809-251-0078)</p>
                  </div>
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">3</p>
                  </div>
                  <div className="column is-11 is-field">
                    <p className="is-size-7">2015, G. LUPERON; 89, esq. RESTAURACION, As=192.00 M2.  por  RD$6,000,000.00  (FREDDY ACEVEDO AL 809-543-9243 //809-345-6119 // 809-462-7633)</p>
                  </div>
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">4</p>
                  </div>
                  <div className="column is-11 is-field">
                    <p className="is-size-7">2018, INDEPENDENCIA esq. 30 DE MARZO, EDIF. (2) N., As=137.95 M2., Ac=253.08 M2. por RD $6,000,000.00 (RAMON E. RODRIGUEZ AL 829-677-3397)</p>
                  </div>
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">5</p>
                  </div>
                  <div className="column is-11 is-field">
                    <p className="is-size-7">2018,  CALLE SALVADOR CUCURULLO, As=218.00 M2., @ RD $29,725.00 C/M2., (F. ABREU AL 809-251-0078)</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Homogenización de las comparables</h2>
              </div>
              <div className="column is-one-quarter">
                <div className="columns is-multiline">
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">#</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Valor</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Área</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">V.U.</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Especulación</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Por dif. de área</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Ubicación</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Factor (frente/fondo)</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Tipo de suelo</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Topografía</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Forma geometrica</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Factor homogenización</p>
                  </div>
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">V.U.H</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold">1</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">7,332,839.47</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">187.92</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,021.07</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.2000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0080</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,333.14</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold">2</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">7,332,839.47</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">187.92</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,021.07</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.2000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0080</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,333.14</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold">3</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">7,332,839.47</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">187.92</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,021.07</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.2000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0080</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,333.14</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold">4</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">7,332,839.47</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">187.92</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,021.07</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.2000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0080</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,333.14</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold">5</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">7,332,839.47</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">187.92</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,021.07</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">0.9500</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.2000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0000</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">1.0080</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="has-text-centered is-size-7">39,333.14</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column is-half">
                <div className="columns is-multiline">
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Promedio (X)</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">40,788.10</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Mediana (Xmed)</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">40,414.61</p>
                  </div>
                </div>
              </div>
              <div className="column is-half">
                <div className="columns is-multiline">
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Desvest. M (S)</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">2,985.06</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Coef. de variación (CV)</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">7.32</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Valor del terreno</h2>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Area total</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">643.32 m²</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Valor unitario promedio en el sector</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">RD$40,414.61</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-6 has-text-weight-bold">Valor total del terreno</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-6 has-text-weight-bold">RD$25,999,529.87</p>
                  </div>
                </div>
              </div>
            </div>
            {/*
              <!-- page 3 end -->
              <!-- page 4 start -->
            */}
            <hr/>
            <h4 className="number_in_circle">4</h4>
            <hr/>

            <div className="divider"></div>

            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Caracteristica de la edificación y mejoras</h2>
              </div>
              <div className="column is-two-fifths">
                <div className="columns is-multiline">
                  <div className="column is-three-fifths is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Cantidad de niveles/pisos</p>
                  </div>
                  <div className="column is-two-fifths is-field">
                    <p className="is-size-7">4</p>
                  </div>
                  <div className="column is-three-fifths is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Tipo de Arquitectura</p>
                  </div>
                  <div className="column is-two-fifths is-field">
                    <p className="is-size-7">Contemporanea</p>
                  </div>
                  <div className="column is-three-fifths is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Estado de conservación</p>
                  </div>
                  <div className="column is-two-fifths is-field">
                    <p className="is-size-7">Excelemte, A</p>
                  </div>
                  <div className="column is-three-fifths is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Calidad de los materiales</p>
                  </div>
                  <div className="column is-two-fifths is-field">
                    <p className="is-size-7">Buenos</p>
                  </div>

                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-three-quarters is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Deseabilidad del inmueble</p>
                  </div>
                  <div className="column is-one-quarter is-field">
                    <p className="is-size-7">Alta</p>
                  </div>
                  <div className="column is-three-quarters is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Acorde con el entorno</p>
                  </div>
                  <div className="column is-one-quarter is-field">
                    <p className="is-size-7">Si</p>
                  </div>
                  <div className="column is-three-quarters is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Habilitada</p>
                  </div>
                  <div className="column is-one-quarter is-field">
                    <p className="is-size-7">Si</p>
                  </div>
                  <div className="column is-three-quarters is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Coeficiente (K)</p>
                  </div>
                  <div className="column is-one-quarter is-field">
                    <p className="is-size-7">0.191</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Edad aparente</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">10 años</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Vida útil estimada</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">50 años</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Edad útil remanente</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">40 años</p>
                  </div>
                  <div className="column is-two-thirds is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">% de vida</p>
                  </div>
                  <div className="column is-one-third is-field">
                    <p className="is-size-7">20%</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Valor de edificación</h2>
              </div>
              <div className="column is-12">
                <div className="columns is-multiline">
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">1</p>
                  </div>
                  <div className="column is-2 is-field">
                    <p className="is-size-7">Sotano</p>
                  </div>
                  <div className="column is-9 is-field">
                    <p className="is-size-7">Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique, sit.</p>
                  </div>
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">2</p>
                  </div>
                  <div className="column is-2 is-field">
                    <p className="is-size-7">Primer piso</p>
                  </div>
                  <div className="column is-9 is-field">
                    <p className="is-size-7">Lorem ipsum dolor sit amet consectetur, adipisicing elit. A tenetur veniam placeat odio. Nihil, cupiditate.</p>
                  </div>
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">3</p>
                  </div>
                  <div className="column is-2 is-field">
                    <p className="is-size-7">Segundo piso</p>
                  </div>
                  <div className="column is-9 is-field">
                    <p className="is-size-7">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vitae perferendis praesentium, nisi illo nobis quis suscipit</p>
                  </div>
                  <div className="column is-1 is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">4</p>
                  </div>
                  <div className="column is-2 is-field">
                    <p className="is-size-7">Tercer piso</p>
                  </div>
                  <div className="column is-9 is-field">
                    <p className="is-size-7">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad eveniet magnam aperiam quidem aspernatur perspiciatis praesentium consequuntur.</p>
                  </div>
                </div>
              </div>
              <div className="column is-half">
                <div className="columns is-multiline">
                  <div className="column is-one-quarter is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Construcción</p>
                  </div>
                  <div className="column is-three-quarters is-field">
                    <p className="is-size-7">Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque quae magnam sequi numquam inventore rem. Sed.</p>
                  </div>
                </div>
              </div>
              <div className="column is-half">
                <div className="columns is-multiline">
                  <div className="column is-one-quarter is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Terminación</p>
                  </div>
                  <div className="column is-three-quarters is-field">
                    <p className="is-size-7">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quidem doloribus explicabo illo tempora adipisci aspernatur voluptatem reprehenderit odit illum!</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Cant.</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">1,676.78</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">VUP</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">RD$ 16,536.89</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold has-text-danger">Dep. Ross-Heidecke</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered has-text-danger">17.19%</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">V. U. Dep</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">13,694.20</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">S-Total</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">22,962,158.34</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Otras mejoras</h2>
              </div>
              <div className="column is-12">
                <div className="columns is-multiline">
                  <div className="column is-one-fifth is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Descripción</p>
                  </div>
                  <div className="column is-four-fifths is-field">
                    <p className="is-size-7">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolores esse illum natus ut iure nihil porro.</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Cant.</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">1.00</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">VUP</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">RD$ 2,850,000.00</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold has-text-danger">Dep. física</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered has-text-danger">20%</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">V. U. Dep</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">2,280,000.00</p>
                  </div>
                </div>
              </div>
              <div className="column is-one-fifth">
                <div className="columns is-multiline">
                  <div className="column is-full is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">S-Total</p>
                  </div>
                  <div className="column is-full is-field">
                    <p className="is-size-7 has-text-centered">2,280,000.00</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Resumen de valores</h2>
              </div>
              <div className="column is-2">
                <div className="columns is-multiline">
                  <div className="column is-12">
                    <p className="is-size-7 has-text-centered">&nbsp;</p>
                  </div>
                  <div className="column is-12 is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Terrenos</p>
                  </div>
                  <div className="column is-12 is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Edificaciones</p>
                  </div>
                  <div className="column is-12 is-label">
                    <p className="has-text-centered is-size-7 has-text-weight-bold">Otras mejoras</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-12 is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold">Valor Total</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7">RD$25,479,539.27</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7">RD$22,962,158.34</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7">RD$2,280,000.00</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-12 is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold has-text-danger">Otros ajustes</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7 has-text-danger">Ajuste por su entorno</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7 has-text-danger">&nbsp;</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7 has-text-danger">&nbsp;</p>
                  </div>
                </div>
              </div>
              <div className="column is-1">
                <div className="columns is-multiline">
                  <div className="column is-12 is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold has-text-danger">%</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7 has-text-danger">2%</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7 has-text-danger">&nbsp;</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7 has-text-danger">&nbsp;</p>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-12 is-label">
                    <p className="is-size-7 has-text-centered has-text-weight-bold">Valor total ajustado</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7">RD$25,479,539.27</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7">RD$22,962,158.34</p>
                  </div>
                  <div className="column is-12 is-field">
                    <p className="has-text-centered is-size-7">RD$2,280,000.00</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-multiline">
              <div className="column">
                <div className="columns is-multiline">
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-7 has-text-weight-bold">Valor total inmueble</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-7">RD$ 50,721,697.62</p>
                  </div>
                  <div className="column is-half is-label">
                    <p className="has-text-right is-size-6 has-text-weight-bold">Valor total ajustado</p>
                  </div>
                  <div className="column is-half is-field">
                    <p className="is-size-6 has-text-weight-bold">RD$ 50,720,000.00</p>
                  </div>
                  <div className="column is-full">
                    <h2 className="has-text-centered is-size-5 has-text-weight-bold">Cincuenta millones setecientos veinte mil pesos con 00/100</h2>
                  </div>
                </div>
              </div>
            </div>
            {/*
              <!-- page 4 end -->
              <!-- page 5 start -->
            */}
            <hr/>
            <h4 className="number_in_circle">5</h4>
            <hr/>

            <div className="divider"></div>
            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Imagenes</h2>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://www.thehousedesigners.com/house-plans/images/AdvSearch2-7263.jpg"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
              <div className="column is-one-quarter">
                <figure className="image is-4by3">
                  <img src="https://bulma.io/images/placeholders/640x480.png"/>
                </figure>
              </div>
            </div>
            {/*
              <!-- page 5 end -->
              <!-- page 6 start -->
            */}
            <hr/>
            <h4 className="number_in_circle">6</h4>
            <hr/>

            <div className="divider"></div>

            <div className="columns is-multiline">
              <div className="column is-full">
                <h2 className="has-text-centered is-size-5 has-text-weight-bold">Documentos</h2>
              </div>
              <div className="column is-12">
                <figure className="image is-4by5">
                  <img src="https://s2.studylib.es/store/data/004772282_1-7e858fa5723839a3c968eab55d9ce1bf.png"/>
                </figure>
              </div>
            </div>
            <div className="divider"></div>

            <div className="columns is-multiline">
              <div className="column is-full">
                <figure className="image is-4by5">
                  <img src="https://s2.studylib.es/store/data/004772282_1-7e858fa5723839a3c968eab55d9ce1bf.png"/>
                </figure>
              </div>
            </div>
            <div className="divider"></div>

            <div className="columns is-multiline">
              <div className="column is-full">
                <figure className="image is-4by5">
                  <img src="https://image.slidesharecdn.com/actadeadjudicacioncniggeoinstitutos-131018020244-phpapp02/95/documento-de-acta-de-adjudicacin-expediente-2013010013000091-del-cnig-1-638.jpg?cb=1382061980"/>
                </figure>
              </div>
            </div>

          </div> {/*<!-- Result -->*/}

        </div> {/*<!-- Container -->*/}
      </div>
    )
  }
}
