import React from 'react'
import Step1 from './step1'
import Step2 from './step2'
import Step3 from './step3'
import Step4 from './step4'
import Step5 from './step5'
import Step6 from './step6'
import Step7 from './step7'
import Step8 from './step8'
import Step9 from './step9'
import Step10 from './step10'
import Status from './status'
import { getStore, addToStore } from '../../../lib/reduxito'
import { prepareProperties, Base } from '../../../lib/utils'
import moment from 'moment'
const os = window.require('os')
const date_format = "dddd D [de] MMMM yyyy"

export default class Steps extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      stepPage: 1,
      proyecto_id: undefined,
      new: false,
      db_data: {},
      step1Keys: ["proyecto","validacion", "descripcion_legal", "descripcion_inmueble", "colindancias"]
    }

    this.changeStep = this.changeStep.bind(this)
    this.print = this.print.bind(this)
    this.save = this.save.bind(this)
    this.loaDataFor = this.loaDataFor.bind(this)

    this.Step1 = React.createRef()
    this.Step2 = React.createRef()
    this.Step3 = React.createRef()
    this.Step4 = React.createRef()
    this.Step5 = React.createRef()
    this.Step6 = React.createRef()
    this.Step7 = React.createRef()
    this.Step8 = React.createRef()
    this.Step9 = React.createRef()
    this.Step10 = React.createRef()
  }

  async loaDataFor(){
    let db_data
    await getStore(async (store) => {
      db_data = await store.getAllModelsDataByProjectId(this.props.proyecto_id, store)
    })
    return db_data
  }

  print(){
    window.print()
  }

  async save(){
    let refState
    let keys
    let proPage = this.state.stepPage
    switch (this.state.stepPage) {
      case 1:
        refState = this.Step1.current
        await this.setState({stepPage: 0})
        keys = this.state.step1Keys
        if (this.state.proyecto_id && this.state.new){
          await getStore( async (d) => {
            //refState.state is the object that contains the data
            refState.state.proyecto_id = this.state.proyecto_id
            let values = prepareProperties(refState.state, d.db_state, keys)
            // TODO: agregar logica de proyect id y mandarle eso a prepareProperties para que se lo ponga a todos los models
            // luego hacer if this.state.proyecto_id update else add

            console.log(values)
            let validacion = new Base(d.db.validacion)
            let descripcion_legal = new Base(d.db.descripcion_legal)
            let descripcion_inmueble = new Base(d.db.descripcion_inmueble)
            let colindancias = new Base(d.db.colindancias)
            let proyecto = new Base(d.db.proyecto)

            validacion.add(values.validacion)
            descripcion_legal.add(values.descripcion_legal)
            descripcion_inmueble.add(values.descripcion_inmueble)
            colindancias.add(values.colindancias)

            values.proyecto.name = values.descripcion_legal.nombre_propietario || "Tasación editada"
            values.proyecto.direccion = values.descripcion_inmueble.direccion_del_inmueble || 'Dirección del cliente'
            values.proyecto.by_pname = values.proyecto.project_name || ''
            values.proyecto.by_prop_name = values.descripcion_legal.nombre_propietario || ''
            values.proyecto.by_cedula = values.descripcion_legal.cedula || ''
            values.proyecto.by_mu_name = values.descripcion_legal.municipio_name || ''
            values.proyecto.by_rnc = values.descripcion_legal.requerido_por_rnc || ''
            values.proyecto.by_prov_name = values.descripcion_legal.provincia_name || ''
            values.proyecto.updated_by_detail = os.userInfo() || ''
            values.proyecto.updated_at = new Date()

            delete values.proyecto.id
            delete values.proyecto.status

            proyecto.update(values.proyecto, d.db, {id: this.state.proyecto_id})
            console.log('UPDATE PROJECT ID IN ADD ------>', this.state.proyecto_id)
            console.log("PROYECT NAME--->", values.proyecto)
            await this.setState({new: false, proyecto_id: this.state.proyecto_id})

            // await d.db.proyecto.where("id").equals(this.state.proyecto_id).modify({
            //   name: values.descripcion_legal.nombre_propietario || "Tasación nueva",
            //   direccion: values.descripcion_inmueble.direccion_del_inmueble || 'Dirección del cliente',
            //   project_name: values.proyecto.project_name,
            //   searchable: `${values.descripcion_legal.cedula}${values.descripcion_legal.requerido_por_rnc}${values.descripcion_legal.nombre_propietario}${values.descripcion_legal.municipio_name}${values.descripcion_legal.provincia_name}`
            // })
          } )

        }else {
          await getStore(async (d) => {
            //refState.state is the object that contains the data
            refState.state.proyecto_id = this.state.proyecto_id
            let values = prepareProperties(refState.state, d.db_state, keys)
            // TODO: agregar logica de proyect id y mandarle eso a prepareProperties para que se lo ponga a todos los models
            // luego hacer if this.state.proyecto_id update else add
            let validacion = new Base(d.db.validacion)
            let descripcion_legal = new Base(d.db.descripcion_legal)
            let descripcion_inmueble = new Base(d.db.descripcion_inmueble)
            let colindancias = new Base(d.db.colindancias)
            let proyecto = new Base(d.db.proyecto)

            values.proyecto.name = values.descripcion_legal.nombre_propietario || "Tasación editada"
            values.proyecto.direccion = values.descripcion_inmueble.direccion_del_inmueble || 'Dirección del cliente'
            values.proyecto.by_pname = values.proyecto.project_name || ''
            values.proyecto.by_prop_name = values.descripcion_legal.nombre_propietario || ''
            values.proyecto.by_cedula = values.descripcion_legal.cedula || ''
            values.proyecto.by_mu_name = values.descripcion_legal.municipio_name || ''
            values.proyecto.by_rnc = values.descripcion_legal.requerido_por_rnc || ''
            values.proyecto.by_prov_name = values.descripcion_legal.provincia_name || ''
            values.proyecto.updated_by_detail = os.userInfo() || ''
            values.proyecto.updated_at = new Date()

            delete values.proyecto.id
            proyecto.update(values.proyecto, d.db, {id: this.state.proyecto_id})
            validacion.update(values.validacion, d.db, {proyecto_id: this.state.proyecto_id}) //where("proyecto_id").equals(this.state.proyecto_id).modify(values.validacion)
            descripcion_legal.update(values.descripcion_legal, d.db, {proyecto_id: this.state.proyecto_id}) //where("proyecto_id").equals(this.state.proyecto_id).modify(values.descripcion_legal)
            descripcion_inmueble.update(values.descripcion_inmueble, d.db, {proyecto_id: this.state.proyecto_id}) //where("proyecto_id").equals(this.state.proyecto_id).modify(values.descripcion_inmueble)
            colindancias.update(values.colindancias, d.db, {proyecto_id: this.state.proyecto_id}) //where("proyecto_id").equals(this.state.proyecto_id).modify(values.colindancias)
            // await d.db.proyecto.where("id").equals(this.state.proyecto_id).modify({
            //   name: values.descripcion_legal.nombre_propietario || "Tasación editada",
            //   direccion: values.descripcion_inmueble.direccion_del_inmueble || 'Dirección del cliente',
            //   searchable: v
            // })
            // await this.setState({proyecto_id: this.state.proyecto_id})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 2:
        refState = this.Step2.current
        await this.setState({stepPage: 0})
        keys = ['sector_inmueble']
        refState.state.proyecto_id = this.state.proyecto_id

        if (this.state.proyecto_id && this.state.new){
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let sector_inmueble = new Base(store.db.sector_inmueble)
            console.log('STEP 2 ADD ---->', values.sector_inmueble)
            sector_inmueble.add(values.sector_inmueble)
            await this.setState({new: false})
          })
        } else {
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let sector_inmueble = new Base(store.db.sector_inmueble)

            console.log('STEP 2 ADD ---->', values.sector_inmueble)

            sector_inmueble.update(values.sector_inmueble, store.db, {proyecto_id: this.state.proyecto_id})
            await this.setState({new: false})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 3:
        refState = this.Step3.current
        await this.setState({stepPage: 0})
        keys = ['servicios_disponibles','vias_acceso']
        refState.state.proyecto_id = this.state.proyecto_id

        if (this.state.proyecto_id && this.state.new){
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let servicios_disponibles = new Base(store.db.servicios_disponibles)
            let vias_acceso = new Base(store.db.vias_acceso)

            console.log('STEP 3 ADD ---->', values)
            servicios_disponibles.add(values.servicios_disponibles)
            vias_acceso.add(values.vias_acceso)
            await this.setState({new: false})
          })
        } else {
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let servicios_disponibles = new Base(store.db.servicios_disponibles)
            let vias_acceso = new Base(store.db.vias_acceso)

            console.log('STEP 3 UPDATE ---->', values)

            servicios_disponibles.update(values.servicios_disponibles, store.db, {proyecto_id: this.state.proyecto_id})
            vias_acceso.update(values.vias_acceso, store.db, {proyecto_id: this.state.proyecto_id})

            await this.setState({new: false})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 4:
        refState = this.Step4.current
        await this.setState({stepPage: 0})
        keys = ['caracteristicas']
        refState.state.proyecto_id = this.state.proyecto_id

        if (this.state.proyecto_id && this.state.new){
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let caracteristicas = new Base(store.db.caracteristicas)
            console.log('STEP 4 ADD ---->', values.caracteristicas)
            caracteristicas.add(values.caracteristicas)
            await this.setState({new: false})
          })
        } else {
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let caracteristicas = new Base(store.db.caracteristicas)

            console.log('STEP 4 UPDATE ---->', values.caracteristicas)

            caracteristicas.update(values.caracteristicas, store.db, {proyecto_id: this.state.proyecto_id})
            await this.setState({new: false})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 5:
        refState = this.Step5.current
        await this.setState({stepPage: 0})
        keys = ['ofertas_ventas_comparables']
        refState.state.proyecto_id = this.state.proyecto_id

        if (this.state.proyecto_id && this.state.new){
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let ofertas_ventas_comparables = new Base(store.db.ofertas_ventas_comparables)
            console.log('STEP 5 ADD ---->', values.ofertas_ventas_comparables)
            ofertas_ventas_comparables.add(values.ofertas_ventas_comparables)
            await this.setState({new: false})
          })
        } else {
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let ofertas_ventas_comparables = new Base(store.db.ofertas_ventas_comparables)

            console.log('STEP 5 UPDATE ---->', values.ofertas_ventas_comparables)

            ofertas_ventas_comparables.update(values.ofertas_ventas_comparables, store.db, {proyecto_id: this.state.proyecto_id})
            await this.setState({new: false})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 6:
        refState = this.Step6.current
        await this.setState({stepPage: 0})
        keys = ['caracteristicas_edificacion_mejora']
        refState.state.proyecto_id = this.state.proyecto_id

        if (this.state.proyecto_id && this.state.new){
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let caracteristicas_edificacion_mejora = new Base(store.db.caracteristicas_edificacion_mejora)
            console.log('STEP 6 ADD ---->', values.caracteristicas_edificacion_mejora)
            caracteristicas_edificacion_mejora.add(values.caracteristicas_edificacion_mejora)
            await this.setState({new: false})
          })
        } else {
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let caracteristicas_edificacion_mejora = new Base(store.db.caracteristicas_edificacion_mejora)

            console.log('STEP 6 UPDATE ---->', values.caracteristicas_edificacion_mejora)

            caracteristicas_edificacion_mejora.update(values.caracteristicas_edificacion_mejora, store.db, {proyecto_id: this.state.proyecto_id})
            await this.setState({new: false})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 7:
        refState = this.Step7.current
        await this.setState({stepPage: 0})
        keys = ['images']
        refState.state.proyecto_id = this.state.proyecto_id

        if (this.state.proyecto_id && this.state.new){
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let images = new Base(store.db.images)
            console.log('STEP 7 ADD ---->', values.images)
            images.add(values.images)
            await this.setState({new: false})
          })
        } else {
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let images = new Base(store.db.images)

            console.log('STEP 7 UPDATE ---->', values.images)

            images.update(values.images, store.db, {proyecto_id: this.state.proyecto_id})
            await this.setState({new: false})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 8:
        refState = this.Step8.current
        await this.setState({stepPage: 0})
        keys = ['documentos']
        refState.state.proyecto_id = this.state.proyecto_id

        if (this.state.proyecto_id && this.state.new){
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let documentos = new Base(store.db.documentos)
            console.log('STEP 7 ADD ---->', values.documentos)
            documentos.add(values.documentos)
            await this.setState({new: false})
          })
        } else {
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let documentos = new Base(store.db.documentos)

            console.log('STEP 7 UPDATE ---->', values.documentos)

            documentos.update(values.documentos, store.db, {proyecto_id: this.state.proyecto_id})
            await this.setState({new: false})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 9:
        refState = this.Step9.current
        await this.setState({stepPage: 0})
        keys = ['pages']
        refState.state.proyecto_id = this.state.proyecto_id

        if (this.state.proyecto_id && this.state.new){
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let pages = new Base(store.db.pages)

            console.log('STEP 9 ADD ---->', values)
            pages.add(values.pages)
            await this.setState({new: false})
          })
        } else {
          await getStore(async (store) => {
            let values = prepareProperties(refState.state, store.db_state, keys)
            let pages = new Base(store.db.pages)

            console.log('STEP 9 UPDATE ---->', values)

            pages.update(values.pages, store.db, {proyecto_id: this.state.proyecto_id})

            await this.setState({new: false})
          })
        }
        await this.setState({stepPage: proPage})
      break;

      case 10:
        refState = this.Step10.current
      break;

      default:
      refState = {noRefState: true}
    }
  }

  async changeStep(action){
    if (action && action === 'next'){
      if (this.state.stepPage === 10) return
      await this.setState({
        stepPage: this.state.stepPage + 1
      })
    }

    if (action && action === 'back'){
      if (this.state.stepPage === 1) return
      await this.setState({
        stepPage: this.state.stepPage - 1
      })
    }
  }

  async componentDidMount(){
    let date = moment().format()
    if (this.props.print){
      await this.setState({stepPage: 10})
    }
    try {
      await getStore( async (store) => {
        console.log("PROYECTO MODULE DB =------>", store.db.proyecto)
        let pro
        if (this.props.proyecto_id){
          pro = await store.db.proyecto.where("id").equals(this.props.proyecto_id).toArray()
        }else {
          pro = await store.db.proyecto.where("name").equals("none").toArray()
        }
        // let pro = await store.db.proyecto.where("name").equals("none").toArray()
        console.log("PROYECTO PRO PRO =------>", pro)
        if (pro[0]){
          console.log(this.props.proyecto_id , pro[0].id)
          await this.setState({
            proyecto_id: pro[0].id, new: this.props.proyecto_id ? false : true,
           })

          console.log(this.state)
        }else {

          console.log("CREATING PROYECT")
          let result = await store.db.proyecto.add({
            name: 'none',
            fecha: moment().format(date_format),
            direccion: 'N/A',
            created_at: date,
            updated_at: date,
            project_name: '',
            searchable: '',
            status: '',
            porcentaje: 0,
            data: {
              step1: '',
              step2: '',
              step3: '',
              step4: '',
              step5: '',
              step6: '',
              step7: '',
              step8: ''
            },
            created_by_detail: os.userInfo() || '', // use whoami using fs to get the username of the pc/mac
            updated_by_detail: `` // use whoami using fs to get the username of the pc/mac
          })
          await this.setState({proyecto_id: result, new: true})
          console.log(result, this.state)
          debugger
        }
      })
    } catch (e) {
      console.log(e, e.message)
    }

    // console.log(this.state)
  }

  render(){
    return(
      <div>
          <nav className="breadcrumb" aria-label="breadcrumbs">
            <ul>
              <li><a onClick={this.props.cancel}>Tasaciones</a></li>
              <li className="is-active"><a href="#" aria-current="page">{this.props.proyecto_id}</a></li>
            </ul>
          </nav>

          <div className="level">
            <div className="level-left">
              <div className="level-item">
                <h1 className="title is-1">
                  Paso {this.state.stepPage} de 10
                </h1>
              </div>
            </div>
            {this.state.stepPage !== 10 ? (
              <div key="1" className="level-right">
                {this.state.stepPage === 1 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 2 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 3 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 4 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 5 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 6 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 7 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                {this.state.stepPage === 8 && this.props.proyecto_id ? (
                  <Status page={this.state.stepPage} id={this.props.proyecto_id} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>
                ):[]}
                <p className="level-item">
                  <button onClick={this.save} className="button is-info is-outlined">
                    <span className="icon is-small">
                      <i className="fas fa-save"></i>
                    </span>
                    <span>Guardar</span>
                  </button>
                </p>
                <p className="level-item">
                  <button className="button is-danger is-outlined" onClick={this.props.cancel}>
                    <span>Cancelar</span>
                    <span className="icon is-small">
                      <i className="fas fa-times"></i>
                    </span>
                  </button>
                </p>
              </div>
            ):(
                <div key="2" className="level-right">
                  <p className="level-item">
                    <button className="button is-success is-outlined" onClick={this.print}>
                      <span className="icon is-small">
                        <i className="fas fa-print"></i>
                      </span>
                      <span>Imprimir o Guardar PDF</span>
                    </button>
                  </p>
                </div>
            )}
          </div>

          <progress className="progress is-info" value={this.state.stepPage * 10} max="100">{this.state.stepPage * 10}%</progress>
          {/*<MuiPickersUtilsProvider utils={MomentUtils}>*/}{/*date picket context*/}
            {this.state.stepPage === 1 ? (<Step1 date_format={date_format} ref={this.Step1} step1Keys={this.state.step1Keys} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>) : []}
            {this.state.stepPage === 2 ? (<Step2 date_format={date_format} ref={this.Step2} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 3 ? (<Step3 date_format={date_format} ref={this.Step3} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 4 ? (<Step4 date_format={date_format} ref={this.Step4} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 5 ? (<Step5 date_format={date_format} ref={this.Step5} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 6 ? (<Step6 date_format={date_format} ref={this.Step6} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 7 ? (<Step7 date_format={date_format} ref={this.Step7} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 8 ? (<Step8 date_format={date_format} ref={this.Step8} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 9 ? (<Step9 date_format={date_format} ref={this.Step9} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' } save={this.save}/>) : []}
            {this.state.stepPage === 10 ? (<Step10 date_format={date_format} ref={this.Step10} db_data={async () => this.props.proyecto_id ? await this.loaDataFor() : '' }/>):[]}
          {/*</MuiPickersUtilsProvider>*/}

          <nav className="pagination is-centered is-fullwidth" role="navigation" aria-label="pagination">
            <a className="pagination-previous" onMouseOver={this.save} onClick={() => this.changeStep('back')}><span className="icon is-small">
                <i className="fas fa-chevron-left"></i>
              </span>Anterior
            </a>
            <a className="pagination-next" onMouseOver={this.save} onClick={() => this.changeStep('next')}>Próximo <span className="icon is-small">
                <i className="fas fa-chevron-right"></i>
              </span>
            </a>
            <ul className="pagination-list">
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 1 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 1})} aria-label="Goto page 1">1</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 2 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 2})} aria-label="Goto page 2">2</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 3 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 3})} aria-label="Goto page 3">3</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 4 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 4})} aria-label="Goto page 4">4</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 5 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 5})} aria-label="Goto page 5">5</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 6 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 6})} aria-label="Goto page 6">6</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 7 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 7})} aria-label="Goto page 7">7</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 8 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 8})} aria-label="Goto page 8">8</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 9 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 9})} aria-label="Goto page 9">9</a></li>
              <li><a onMouseOver={this.save} className={`pagination-link ${this.state.stepPage === 10 ? 'is-current' : ''}`} onClick={() => this.setState({stepPage: 10})} aria-label="Goto page 10">10</a></li>
            </ul>
          </nav>
      </div>
    )
  }
}
