import React from 'react'

export default class MapView extends React.Component {
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div className="container">
          <div className="level">
              <div className="level-left">
                  <div className="level-item">
                      <h1 className="title is-1">
                          Propiedades en el Mapa
                      </h1>
                  </div>
              </div>
              <div className="level-right">
              </div>
          </div>
          <article className="panel">
              <div className="panel-block">
                  <p className="control has-icons-left">
                      <input className="input" type="text" placeholder="Buscar propiedad"/>
                      <span className="icon is-left">
                          <i className="fas fa-search" aria-hidden="true"></i>
                      </span>
                  </p>
              </div>
              <div className="panel-block">
                  <div style={{width: '100%'}}>
                      <iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&height=600&hl=es&q=Les%20Rambles%2C%201%20Barcelona%2C%20Spain+(Mi%20nombre%20de%20egocios)&ie=UTF8&t=&z=14&iwloc=B&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">

                      </iframe>
                  </div>
              </div>
          </article>
      </div>
    )
  }
}
