import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './pages/index';
import * as serviceWorker from './serviceWorker';
import setup from './pages/lib/db'
import { getStore, addToStore } from './pages/lib/reduxito'
import "react-datepicker/dist/react-datepicker.css"
import 'tui-image-editor/dist/tui-image-editor.css'

(async () => {
  await setup()
  await getStore(async (s) => {
    let {a, db} = await s.init()
    await addToStore({ dbs: a,  db: db })
  })
})()

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
